import { combineReducers } from 'redux'
import { all } from 'redux-saga/effects'

import { STAGE } from './types';
import { 
    watchGetStageAsync,
    watchSetStageAsync
} from './sagas';


const stageReducer = (state={}, action) => {
    switch (action.type) {
        case STAGE.GET.INIT:
            return { ...state, loading: true };
        case STAGE.GET.SUCCESS:
            return { ...state, loading: false };
        case STAGE.GET.ERROR:
            return { ...state, loading: false };
        case STAGE.SET.INIT:
            return { ...state, loading: true };
        case STAGE.SET.SUCCESS:
            return { ...state, loading: false, stage: action.payload };
        case STAGE.SET.ERROR:
            return { ...state, loading: false };
        default:
            return state
    }
}

export function* rootSaga() {
    yield all([
      watchGetStageAsync(),
      watchSetStageAsync(),
    ])
}

export const reducer = combineReducers({ 
    stage: stageReducer
})
