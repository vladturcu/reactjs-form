import { STAGE } from './types.js';

export function getStage(data) {
    return { type: STAGE.GET.ASYNC, data }
}

export function setStage(data) {
    return { type: STAGE.SET.ASYNC, data }
}