import { put, takeEvery} from 'redux-saga/effects'
import { STAGE } from './types';

export function* getStage({ data }) {
    yield put({type:  STAGE.GET.INIT}) 
    try {
        yield put({type: STAGE.GET.SUCCESS, payload: data}) 
    } catch (err) {
        yield put({type: STAGE.GET.ERROR}) 
    }
}

export function* setStage({ data }) {
    yield put({type: STAGE.SET.INIT}) 
    try {
        yield put({type: STAGE.SET.SUCCESS, payload: data}) 
    } catch (err) {
        yield put({type: STAGE.SET.ERROR}) 
    }
}

export function* watchGetStageAsync() {
    yield takeEvery(STAGE.GET.ASYNC, getStage)
}

export function* watchSetStageAsync() {
    yield takeEvery(STAGE.SET.ASYNC, setStage)
}