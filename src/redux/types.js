
export const STAGE = {
    GET: {
        ASYNC: "GET_STAGE_ASYNC",
        INIT: "GET_STAGE_INIT",
        SUCCESS: "GET_STAGE_SUCCESS",
        ERROR: "GET_STAGE_ERROR",
    },
    SET: {
        ASYNC: "SET_STAGE_ASYNC",
        INIT: "SET_STAGE_INIT",
        SUCCESS: "SET_STAGE_SUCCESS",
        ERROR: "SET_STAGE_ERROR",
    }
}

