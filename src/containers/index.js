import React, { useState, useEffect } from 'react';
import { Page } from '../components'

import { connect } from 'react-redux';
import { getStage, setStage } from '../redux/actions';

export default connect(state => state, { getStage, setStage })((props) => {
    const [stage, setStage] = useState(null);
    
    const nextStage = () => {
        props.setStage(stage + 1);
    }

    useEffect(() => {
        props.setStage(1);
    }, [])

    useEffect(() => {
        props.getStage();
        return setStage(props.stage.stage);
    }, [props.stage.stage])

    return (<Page>
        {stage ? stage : 'Loading...'}
        <button onClick={() => nextStage()}>Next stage</button></Page>)
});
