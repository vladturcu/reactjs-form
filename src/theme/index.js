export default {
    mixins: {
        boxShadow: `
            -webkit-box-shadow: 0 8px 17px 2px rgba(0,0,0,0.04), 0 3px 14px 2px rgba(0,0,0,0.03), 0 5px 5px -3px rgba(0,0,0,0.02);
            -moz-box-shadow: 0 8px 17px 2px rgba(0,0,0,0.04), 0 3px 14px 2px rgba(0,0,0,0.03), 0 5px 5px -3px rgba(0,0,0,0.02);
            -o-box-shadow: 0 8px 17px 2px rgba(0,0,0,0.04), 0 3px 14px 2px rgba(0,0,0,0.03), 0 5px 5px -3px rgba(0,0,0,0.02);
            box-shadow: 0 8px 17px 2px rgba(0,0,0,0.04), 0 3px 14px 2px rgba(0,0,0,0.03), 0 5px 5px -3px rgba(0,0,0,0.02);
        `
    },
    colors: {
        white: '#ffffff',
        black: '#000000',
        green: '#0dbf73',
        navy: '#0a0d36',
        blue: '#0d66e5',
        pink: 'rgba(252, 74, 186, 0.95)',
        backgroundLight: '#f7f7f9'
    }
}