import styled from 'styled-components';

export const StyledPage = styled.div`
    width: 60%;
    height: 70%;
    border-radius: 10px;
    background-color: ${props => props.theme.colors.white};
    ${props => props.theme.mixins.boxShadow}
`;