import React from 'react';
import { StyledPage } from './styles';

export default (props) => {
    return (
        <StyledPage>
            {props.children}
        </StyledPage>
    )
}