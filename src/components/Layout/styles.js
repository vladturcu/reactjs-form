import styled from 'styled-components';

export const StyledLayout = styled.div`
    width: 100vw;
    height: 100vh;
    background-color: ${props => props.theme.colors.backgroundLight};
    display: flex;
    justify-content: center;
    align-items: center;
`;