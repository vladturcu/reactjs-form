import React from 'react';
import { StyledLayout } from './styles';

export default (props) => {
    return (
        <StyledLayout>
            {props.children}
        </StyledLayout>
    )
}